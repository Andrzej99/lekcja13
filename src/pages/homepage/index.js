import React,{useContext, useEffect} from 'react';
import User from '../../components/User';
import Oponent from './../../components/Oponent';
//na początku importujemy UserStatsProvider
import { UserStatsProvider,OponentStatsProvider, CharsStats,GameState } from './../../context';

import Form from './../../components/Form';

const Homepage = () => {

 //możemy wyświetlić parametry z CharsStats
 const { name, str, hp, speed,addStr} = useContext(CharsStats);
 const {isName,setName}=useContext(GameState);

 useEffect(()=> {
console.log('homepage: ',isName)
 },[isName])
//console.log('isName:',isName);

    return (
        <div>
           {isName ? (
           <div className="game__wrapper">
            <UserStatsProvider>
                <User poziom="10"/>
            </UserStatsProvider>

            <OponentStatsProvider>
            <Oponent poziom="10" alive/>
            </OponentStatsProvider>
        </div>
    ) : (
        <Form/>
    )}
    </div>
    );
    // jeśli nie stworzonego providera,(np. dla drugiego Usera), to pobieran sa domyslne wartości z 
    // charsStat. Wartości w CharsStat bedą się zmieniać. UserStatsProvider bedzie miał inne, a OponentStatsProvide
    // będzie mial inne jeśli owprapowaliśmy w provider, to z niego 
    //będą pobierane wartośći.

    


}

export default Homepage;
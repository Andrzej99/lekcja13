
import './App.css';
import React from 'react';
import { GameApp } from './context';
import Homepage from './pages';

function App() {


     
  return (
    <div >
      <GameApp>
        <Homepage/>
      </GameApp>
    </div>
  );
}

export default App;

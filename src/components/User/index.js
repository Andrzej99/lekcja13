import './styles.css';
import { useContext, useEffect } from 'react';
import { CharsStats } from './../../context'
import TableRow from './../TableRow';
/* Odbieranie dzieci komponentu User*/


const User = (props) => {
    const { name, str, hp, speed,addStr,addName } = useContext(CharsStats);
    const myStats={name,str,hp,speed};

    useEffect(()=>{
        addName(localStorage.getItem('name'))
    },[])
    

    return (
        <div className="col-6">
            <h1>{props.poziom}</h1>
            <TableRow {...myStats}/>
            <div onClick={()=>addStr(str+1)}>add str +1</div> 

        </div>
    )


}


export default User;
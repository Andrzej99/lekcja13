import React, { createContext,useState } from 'react';  //useState jest hookiem
// CharsStats przechowuje statyski
// po zadeklarowaniu wartości w UserStatsProvider i OponentStatsProvider wartości w ustawiamy na zero
export const CharsStats=createContext({
    name: "",
    str: 0,
    hp: 0,
    speed: 0,
    addStr:item => {}
})

export const GameState=createContext({
    isName:false,
 //   setName:item => { this.isName=item }   
    setName:item => {}
})

export const GameApp=({children})=> {
    const [isName,setName]=useState(false);
    return (
        <GameState.Provider
        value={{
            isName,
            setName
        }}
        >
            {children}
        </GameState.Provider>
    )
}

export const UserStatsProvider = ({ children }) => {
    const name = 'Glololo';
//    const str = 10;
const [str,addStr]=useState(10);
    const hp = 12;
    const speed = 2;
    return (
        <CharsStats.Provider
            value={{
                name,
                str,
                hp,
                speed,
                addStr
            }}
        >
            {children}
        </CharsStats.Provider>

    )
}

//w Demonie nie przekazujemy prędkości
export const OponentStatsProvider = ({ children }) => {
    const name = 'Demon';
    const str = 10;
    const hp = 12;
    return (
        <CharsStats.Provider
            value={{
                name,
                str,
                hp
            }}
        >
            {children}
        </CharsStats.Provider>

    )
}